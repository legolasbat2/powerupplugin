// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CPPACMAN_CPPacmanGameModeBase_generated_h
#error "CPPacmanGameModeBase.generated.h already included, missing '#pragma once' in CPPacmanGameModeBase.h"
#endif
#define CPPACMAN_CPPacmanGameModeBase_generated_h

#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_SPARSE_DATA
#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_RPC_WRAPPERS
#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPPacmanGameModeBase(); \
	friend struct Z_Construct_UClass_ACPPacmanGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACPPacmanGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPPacman"), NO_API) \
	DECLARE_SERIALIZER(ACPPacmanGameModeBase)


#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACPPacmanGameModeBase(); \
	friend struct Z_Construct_UClass_ACPPacmanGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACPPacmanGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPPacman"), NO_API) \
	DECLARE_SERIALIZER(ACPPacmanGameModeBase)


#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPPacmanGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPPacmanGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPacmanGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPacmanGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPacmanGameModeBase(ACPPacmanGameModeBase&&); \
	NO_API ACPPacmanGameModeBase(const ACPPacmanGameModeBase&); \
public:


#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPPacmanGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPPacmanGameModeBase(ACPPacmanGameModeBase&&); \
	NO_API ACPPacmanGameModeBase(const ACPPacmanGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPPacmanGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPPacmanGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPPacmanGameModeBase)


#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_12_PROLOG
#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_SPARSE_DATA \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_RPC_WRAPPERS \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_INCLASS \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_SPARSE_DATA \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	CPPacman_Source_CPPacman_CPPacmanGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPPACMAN_API UClass* StaticClass<class ACPPacmanGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPPacman_Source_CPPacman_CPPacmanGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
