// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EFoodType : uint8;
#ifdef CPPACMAN_Food_generated_h
#error "Food.generated.h already included, missing '#pragma once' in Food.h"
#endif
#define CPPACMAN_Food_generated_h

#define CPPacman_Source_CPPacman_Food_h_16_DELEGATE \
struct _Script_CPPacman_eventFoodEatenEvent_Parms \
{ \
	EFoodType FoodType; \
}; \
static inline void FFoodEatenEvent_DelegateWrapper(const FMulticastScriptDelegate& FoodEatenEvent, EFoodType FoodType) \
{ \
	_Script_CPPacman_eventFoodEatenEvent_Parms Parms; \
	Parms.FoodType=FoodType; \
	FoodEatenEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define CPPacman_Source_CPPacman_Food_h_21_SPARSE_DATA
#define CPPacman_Source_CPPacman_Food_h_21_RPC_WRAPPERS
#define CPPacman_Source_CPPacman_Food_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define CPPacman_Source_CPPacman_Food_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFood(); \
	friend struct Z_Construct_UClass_AFood_Statics; \
public: \
	DECLARE_CLASS(AFood, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPPacman"), NO_API) \
	DECLARE_SERIALIZER(AFood)


#define CPPacman_Source_CPPacman_Food_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAFood(); \
	friend struct Z_Construct_UClass_AFood_Statics; \
public: \
	DECLARE_CLASS(AFood, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPPacman"), NO_API) \
	DECLARE_SERIALIZER(AFood)


#define CPPacman_Source_CPPacman_Food_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFood(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFood) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFood); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFood); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFood(AFood&&); \
	NO_API AFood(const AFood&); \
public:


#define CPPacman_Source_CPPacman_Food_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFood(AFood&&); \
	NO_API AFood(const AFood&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFood); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFood); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFood)


#define CPPacman_Source_CPPacman_Food_h_21_PRIVATE_PROPERTY_OFFSET
#define CPPacman_Source_CPPacman_Food_h_18_PROLOG
#define CPPacman_Source_CPPacman_Food_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Source_CPPacman_Food_h_21_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Source_CPPacman_Food_h_21_SPARSE_DATA \
	CPPacman_Source_CPPacman_Food_h_21_RPC_WRAPPERS \
	CPPacman_Source_CPPacman_Food_h_21_INCLASS \
	CPPacman_Source_CPPacman_Food_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPPacman_Source_CPPacman_Food_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Source_CPPacman_Food_h_21_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Source_CPPacman_Food_h_21_SPARSE_DATA \
	CPPacman_Source_CPPacman_Food_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	CPPacman_Source_CPPacman_Food_h_21_INCLASS_NO_PURE_DECLS \
	CPPacman_Source_CPPacman_Food_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPPACMAN_API UClass* StaticClass<class AFood>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPPacman_Source_CPPacman_Food_h


#define FOREACH_ENUM_EFOODTYPE(op) \
	op(EFoodType::Regular) \
	op(EFoodType::PowerUp) 

enum class EFoodType : uint8;
template<> CPPACMAN_API UEnum* StaticEnum<EFoodType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
