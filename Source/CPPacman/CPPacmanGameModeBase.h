// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CPPacmanGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CPPACMAN_API ACPPacmanGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
