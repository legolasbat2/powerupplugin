// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPowerUpEffect;
#ifdef POWERUPSPLUGIN_PowerUpUser_generated_h
#error "PowerUpUser.generated.h already included, missing '#pragma once' in PowerUpUser.h"
#endif
#define POWERUPSPLUGIN_PowerUpUser_generated_h

#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_SPARSE_DATA
#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddPowerUps);


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddPowerUps);


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPowerUpUser(); \
	friend struct Z_Construct_UClass_UPowerUpUser_Statics; \
public: \
	DECLARE_CLASS(UPowerUpUser, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerUpsPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPowerUpUser)


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPowerUpUser(); \
	friend struct Z_Construct_UClass_UPowerUpUser_Statics; \
public: \
	DECLARE_CLASS(UPowerUpUser, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerUpsPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPowerUpUser)


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPowerUpUser(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPowerUpUser) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPowerUpUser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPowerUpUser); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPowerUpUser(UPowerUpUser&&); \
	NO_API UPowerUpUser(const UPowerUpUser&); \
public:


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPowerUpUser(UPowerUpUser&&); \
	NO_API UPowerUpUser(const UPowerUpUser&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPowerUpUser); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPowerUpUser); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPowerUpUser)


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_PRIVATE_PROPERTY_OFFSET
#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_12_PROLOG
#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_SPARSE_DATA \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_RPC_WRAPPERS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_INCLASS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_SPARSE_DATA \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_INCLASS_NO_PURE_DECLS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POWERUPSPLUGIN_API UClass* StaticClass<class UPowerUpUser>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpUser_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
