// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPowerUpUser;
#ifdef POWERUPSPLUGIN_PowerUpComponent_generated_h
#error "PowerUpComponent.generated.h already included, missing '#pragma once' in PowerUpComponent.h"
#endif
#define POWERUPSPLUGIN_PowerUpComponent_generated_h

#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_SPARSE_DATA
#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execActivatePowerUp); \
	DECLARE_FUNCTION(execGetPowerUp);


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execActivatePowerUp); \
	DECLARE_FUNCTION(execGetPowerUp);


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPowerUpComponent(); \
	friend struct Z_Construct_UClass_UPowerUpComponent_Statics; \
public: \
	DECLARE_CLASS(UPowerUpComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerUpsPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPowerUpComponent)


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUPowerUpComponent(); \
	friend struct Z_Construct_UClass_UPowerUpComponent_Statics; \
public: \
	DECLARE_CLASS(UPowerUpComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerUpsPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPowerUpComponent)


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPowerUpComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPowerUpComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPowerUpComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPowerUpComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPowerUpComponent(UPowerUpComponent&&); \
	NO_API UPowerUpComponent(const UPowerUpComponent&); \
public:


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPowerUpComponent(UPowerUpComponent&&); \
	NO_API UPowerUpComponent(const UPowerUpComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPowerUpComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPowerUpComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPowerUpComponent)


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_PRIVATE_PROPERTY_OFFSET
#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_11_PROLOG
#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_SPARSE_DATA \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_RPC_WRAPPERS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_INCLASS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_SPARSE_DATA \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_INCLASS_NO_PURE_DECLS \
	CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POWERUPSPLUGIN_API UClass* StaticClass<class UPowerUpComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPPacman_Plugins_PowerUpsPlugin_Source_PowerUpsPlugin_Public_PowerUpComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
